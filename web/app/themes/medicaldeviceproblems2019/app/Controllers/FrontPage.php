<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{

    public function variables() {

        return $variables = [
            "intro" => get_field('intro'),
            "definition" => get_field('definition'),
            "quote" => get_field('quote'),
            "quote_byline" => get_field('quote_byline'),
            "update" => get_field('update'),
            "youtube_video" => get_field('youtube_video')
        ];
	}
	
}
