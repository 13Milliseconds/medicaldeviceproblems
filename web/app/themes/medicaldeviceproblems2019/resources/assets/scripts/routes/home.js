//import Slick from 'slick-carousel'

export default {
  init() {
    // JavaScript to be fired on the home page

    var windowHeight = $(window).height();
    var widthHeigth = $(window).width() / 8;
    var documentHeight = $(document).height() - windowHeight;
    var DeviceCarouselWrapper = $('.deviceGroup');
    var oneinten = $('#oneinten');
    var tenStops = oneinten.offset().top;
    var tenAppears = oneinten.offset().top - windowHeight;
    var storyPlayer = $('.storyPlayer');
    var ratio = 1;
    var tenElement = $('.ten');
    var oneElement = $('.one');
    var progressBar = $('#progressBar');
    var months = $('#months');
    var scroll = $(window).scrollTop();
    var progress = scroll / documentHeight * 100;

    var padding = $(window).innerHeight() / 2
    oneinten.css('padding', padding.toString() + 'px 0')


    $('.parallax-mirror').addClass('landing');

    var scrollAnimation = function () {
      scroll = $(window).scrollTop();
      progress = scroll / documentHeight * 100;
      tenStops = oneinten.offset().top;
      tenAppears = oneinten.offset().top - windowHeight;
      progressBar.width(progress.toString() + '%')

      //Header Color
      if (scroll > windowHeight) $('#mainheader').removeClass('dark');
      else $('#mainheader').addClass('dark');

      if (scroll > tenAppears && scroll <= tenStops) {
        ratio = (1 - (scroll - tenAppears) / (tenStops - tenAppears)) * windowHeight / 2;
        tenElement.css('transform', 'translateY( ' + ratio + 'px )')
        oneElement.css('transform', 'translateY( -' + ratio + 'px )')
      }
      else {
        tenElement.css('transform', 'translateY(0)')
        oneElement.css('transform', 'translateY(0)')
      }
    }

    //Months Animation
    months.waypoint(function(direction) {
      if (direction === 'down') $('.animation', months).addClass('launched')
      else $('.animation', months).removeClass('launched')
    }, {
      offset: '70%',
    })

    $('.counter').counterUp({
      time: 2000,
    });

    //Examples
    //Slick
    $(document).ready(function(){

      DeviceCarouselWrapper.slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 2,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });

      homeResize();
    });

    //Init
    DeviceCarouselWrapper.on('init', function(){
      homeResize();
      $('#examples').css('opacity', 1);
    });
    // Next
    $('.slickNav .next').click(function(){
      DeviceCarouselWrapper.slick('slickNext');
    });
    $('.slickNav .prev').click(function(){
      DeviceCarouselWrapper.slick('slickPrev');
    });
    DeviceCarouselWrapper.on('init reInit afterChange', function(event, slick, currentSlide){
        var i = (currentSlide ? currentSlide : 0);
        var slideSet = slick.activeBreakpoint ? slick.breakpointSettings[slick.activeBreakpoint] : slick.options
        $('.slickNav .next').removeClass('inactive')
          $('.slickNav .prev').removeClass('inactive')
        if (i === 0) $('.slickNav .prev').addClass('inactive')
        if (slideSet.slidesToShow + 1 + i > slick.slideCount) $('.slickNav .next').addClass('inactive')
    });

    ///////////////
    //One in Ten Animation
    ///////////////

    $(window).scroll(function(){
      scrollAnimation();
    });

    oneinten.waypoint(function(direction) {
      if (direction === 'down') oneinten.addClass('static') 
      else oneinten.removeClass('static')
    }, {
      offset: '0%',
    })

    oneinten.waypoint(function(direction) {
      if (direction === 'down') {
        $('.partOne').fadeOut(300, function(){$('.partTwo').fadeIn(300)});
      }
      else {
        $('.partTwo').fadeOut(300, function(){$('.partOne').fadeIn(300)});
      }
    }, {
      offset: '-100%',
    })

    ///////////////
    //Story
    ///////////////

    $('.launch').click(function(){
      storyPlayer.fadeIn();
      $('iframe', storyPlayer)[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
    });
    storyPlayer.click(function(){
      storyPlayer.fadeOut();
      $('iframe', storyPlayer)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    });
    storyPlayer.on('click', 'div', function(e){
      e.stopPropagation();
    });
    $('.close').click(function(){
      storyPlayer.fadeOut();
      $('iframe', storyPlayer)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    });


    ///////////////
    // Resize the window
    ///////////////

    var homeResize = function(){
      windowHeight = $(window).height();
      widthHeigth = $(window).width() / 8;
      documentHeight = $(document).height() - windowHeight;
      tenAppears = oneinten.offset().top - windowHeight;
      tenStops = oneinten.offset().top;
      padding = $(window).innerHeight() / 2;
      oneinten.css('padding', padding.toString() + 'px 0')
      progressBar.css('top', $('header#mainheader').innerHeight());
      console.log($('#mainheader').innerHeight())

      //Resize the month graph
      $('.graph polyline').attr('points', '0 180, ' + widthHeigth + ' 100, ' + 3 * widthHeigth + ' 150, ' + 5 * widthHeigth + ' 70, ' + 7 * widthHeigth + ' 50, ' + 8 * widthHeigth + ' 5')

      //Check that the animation is at the right spoty
      scrollAnimation();
    }

    $(window).resize(homeResize).resize();

    $(document).ready(function(){
      setTimeout(homeResize, 1000);
    });


  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
