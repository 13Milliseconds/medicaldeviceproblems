import ScrollReveal from 'scrollreveal'

export default {
  init() {
    // JavaScript to be fired on all pages
    
     //Top menu
     $('.hamburger').click(function(){
      $('#sideMenu').addClass('active');
    });
    $('#sideMenu header a').click(function(){
      $('#sideMenu').removeClass('active');
    });
    $('.shareButton').click(function () {
      $('.rightSide').toggleClass('active');
    });

    $(window).on('load', function() {
      if ($('video').length){
        $('source').each(function (index, obj) {
          $(obj).attr('src', $(obj).attr('temp-src'));
        })
        $('video')[0].load();
        $('video')[0].play();
      }
    });

    //ScrollReveal
    window.sr = ScrollReveal();
    window.sr.reveal('.reveal');

    /////////////////
    // STEPS
    /////////////////
    if ($('.steps.numbered')){
      $('.steps.numbered').each(function(index, group){
        $('.step', group).each(function(index, obj){
          var number = index + 1;
          $('.number span', obj).text(number);
        })
      });
    }


    /////////////////
    // DRAWERS
    ////////////////
    if ($('.drawers').length){
      //The actual functions

      $('.drawers').find('.title').click(function(event) {
        let drawer = $(event.target).parents('.drawer').length ? $(event.target).parents('.drawer') : $(event.target).parents('.deviceDrawer');
        console.log(drawer);
        if (drawer.hasClass('active')){
          $('.drawer.active, .deviceDrawer.active').removeClass('active')
        } else {
          $('.drawer.active, .deviceDrawer.active').removeClass('active')
          drawer.addClass('active');
        }
        drawer.find('i').toggleClass('fa-caret-down');
        drawer.find('i').toggleClass('fa-caret-up');
        // Stop bubbling and default behavior for jQuery event
        return false;
      });
    }

    //Smooth Scrolling
  // Select all links with hashes
  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 80,
        }, 1000, function() {
          // Callback after animation
        });
      }
    }
  });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
