@if ( has_post_thumbnail() )
    <div class="banner" 
        data-parallax="scroll" 
        data-image-src="{!! get_the_post_thumbnail_url() !!}">
    </div>
@endif
