@if( have_rows('content') )
<div class="flexible-content">
@while( have_rows('content') )
@php the_row(); @endphp

    @if( get_row_layout() == 'photo' )

    <div class="photo">
        <img src="{{ get_sub_field('image')['url'] }}" width="100%" />
    </div>

    @elseif(get_row_layout() == 'youtube_video')

    <div class="video">
        <div class="embed-container">
            {!! get_sub_field('video') !!}
        </div>
    </div>

    @elseif(get_row_layout() == 'title')
    
    <div class="title">
        <h3>{{ get_sub_field('text') }}</h3>
    </div>

    @elseif(get_row_layout() == 'list')

        @if( have_rows('list_items') )
        <div class="steps">
        @while( have_rows('list_items') )
        @php the_row(); @endphp
            <div class="step reveal row">
                <div class="number col-1">
                    <span></span>
                </div>
                <div class="text col-11">
                    {!! get_sub_field('content') !!}
                </div>
            </div>

        @endwhile
        </div>
        @endif
    @endif
    @endwhile
    </div>
    @endif