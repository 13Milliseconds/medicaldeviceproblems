<article @php post_class() @endphp >
  <div class="thumbnail">
    {!! the_post_thumbnail('medium'); !!}
  </div>
  <div class="text-content">
    <a href="{{ the_permalink() }}">
      <h4 class="entry-title">{!! get_the_title() !!}</h4>
    </a>
    <div class="time"><time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></div>
    @php the_excerpt() @endphp
  </div>
</article>
