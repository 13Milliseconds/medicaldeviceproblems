<article @php post_class() @endphp>
  <header class="wrap">
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    @include('partials/entry-meta')
  </header>

  <div class="featured-img">
      {!! the_post_thumbnail(); !!}
  </div>

  <div class="entry-content wrap">
    @php the_content() @endphp
  </div>

  <footer>
    <a href="/news" class="button">Back to all news</a>
  </footer>

</article>
