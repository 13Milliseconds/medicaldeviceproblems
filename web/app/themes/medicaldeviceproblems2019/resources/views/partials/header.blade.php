<header class="banner" id="mainheader">
  <div class="logo">
    <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
  </div>

    <div class="rightSide">
        <a href="/about">About</a>
        <a class="button shareButton">Share</a>
    <div id="share">
        <a id="facebookShare" href="#">Facebook</a>
        <a href="https://twitter.com/home?status=Learn%20more%20about%20medical%20devices!%20http%3A//www.medicaldeviceproblems.com" target="_blank">Twitter</a>
        <a href="mailto:?subject=Medical%20Devices%20Problems&body=Learn about medical devices problems at http://medicaldevicesproblems.com">Email</a>
        <script>
        document.getElementById('facebookShare').onclick = function(e) {
          e.preventDefault();
          FB.ui({
            method: 'share',
            display: 'popup',
            href: 'http://www.medicaldeviceproblems.com',
          }, function(response){});
        }
        </script>
    </div>
    <div class="hamburger"><i class="fa fa-bars"></i></div>
  </div>

  <div id="sideMenu">
      <header>
        <a class="button">Close</a>
      </header>
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
  </div>
</header>
