@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @if ( has_post_thumbnail() )
      <div class="banner" data-parallax="scroll" data-image-src="{!! get_the_post_thumbnail_url() !!}"></div>
    @endif

    <section id="actIntro">
        <div class="wrap">
          <h1>{!! App::title() !!}</h1>
          @php the_content() @endphp
        </div>
    </section>

  <section id="actContent">
    <div class="wrap">
        <div class="row">
        @php 
          if( have_rows('action_blocks') ): 
          while( have_rows('action_blocks') ): 
          the_row(); 
          $page = get_sub_field('page');
          $description = get_sub_field('description');
          $link_text = get_sub_field('link_text');
          @endphp

          <div class="content-block reveal col-6">
              <div class="inner">
                  <header>
                      {{ $page->post_title }}
                  </header>
                  <div class="text">
                      {{ $description }}
                  </div>
                  <div class="link">
                      <a href="{{ get_permalink($page) }}" class="button">{{ $link_text }}</a>
                  </div>
              </div>
          </div>
            
          @php endwhile; endif; @endphp
          </div>
      </div>
    </section>

    @endwhile
@endsection