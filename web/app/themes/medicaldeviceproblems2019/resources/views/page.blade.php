@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <section id="pageContent" class="@php the_field('color_theme') @endphp">
      <div class="wrap">
        @include('partials.content-page') 
        @include('partials.interactive-content-page') 
      </div>
    </section>
  @endwhile
@endsection
