@extends('layouts.app')

@section('content')
  <div class="main-content home">

  <section id="intro">
    <div class="wrap">
      <h2>{!! $variables['intro'] !!}</h2>
    <a class="button" href="/take-action">I've been affected by a device</a>    
    <a class="button" href="/resources">I'm looking to learn more about devices</a>    
    </div>
    <div class="more">
      <a href="#months">
      Learn more
      <div class="arrow">
        <img src="@asset('images/arrow.svg')"/>
      </div>
      </a>
    </div>
  </section>


  <section id="months">
  <div class="wrap">
    <h2>In the first 4 months of 2018, they have been linked to the deaths of <strong><span class="counter">2,368</span></strong> patients.</h2>
  </div>
  <div class="animation">
    <div class="month">January</div>
    <div class="month">February</div>
    <div class="month">March</div>
    <div class="month">April</div>
    <div class="bar"></div>
    <div class="graph">
      <svg height="200" width="100%">
        <polyline points="0 200, 250 100, 500 150, 750 120, 1000 5"
               stroke="#40D764" fill="transparent" stroke-width="5" />
      </svg>
    </div>
  </div>
  </section>


  <section id="definition" class="row">
    <div class="col-md-6">
      <h2>What is a <strong>medical device</strong>?</h2>
      {!! $variables['definition'] !!}
    </div>
  </section>


  <section id="examples">
  <div class="title">
    <h3>Examples</h3>
    <a href="/resources#findADevice">Learn more about these devices</a>
  </div>
    <div class="slickNav row">
      <div class="col-6">
        <a class="prev"><img src="@asset('images/arrow.svg')"/></a>
      </div>
      <div class="col-6">
        <a class="next"><img src="@asset('images/arrow.svg')')"/></a>
      </div>
    </div>
    <div class="deviceGroup">
      @php 
      if( have_rows('examples') ): 
      while( have_rows('examples') ): 
      the_row(); 
      $image = get_sub_field('image');
      $title = get_sub_field('title');
      $content = get_sub_field('content');
      @endphp
        <div class="device">
            <div class="wrap">
          <img src="<?php echo $image ?>" />
          <div class="content">
            <h4>@php echo $title @endphp</h4>
            <p>@php echo $content @endphp</p>
          </div>
        </div>
        </div>
	    @php endwhile; endif; @endphp
    </div>
  </section>


  <section id="oneinten">
    <div class="wrap">
      <div class="partOne">
        <div class="firstline"><div class="one">One</div> in <div class="ten">Ten</div></div>
        <div class="secondline">Americans will be implanted with at least one device over their lifetime.</div>
      </div>
        <div class="partTwo">The overwhelming majority of high-risk implanted devices <strong>never</strong> require a single clinical trial.</div>
    </div>
    <div class="whiteBlur"></div>
  </section>


  <section id="story"  data-parallax="scroll" data-image-src="@asset('images/story-bg.jpg')" data-bleed="10">
    <div class="wrap">
      <div class="quote">{!! $variables['quote'] !!}</div>
        <div class="byline">
          {!! $variables['quote_byline'] !!}
    </div>
    <a class="launch"><img src="@asset('images/play-button.svg')" /></a>
    </div>
    
    
    <div class="storyPlayer">
      <div class="player-wrap">
        <a class="button close">Close</a>
        <div class="embed-container">
          {!! $variables['youtube_video'] !!}
        </div>
      </div>
    </div>

  </section>
  <section id="update" data-parallax="scroll" data-image-src="@asset('images/update-bg.jpg')">
    <div class="wrap">
        <div class="row">
      <div class="align-self-end col-md-6" >
          {!! $variables['update'] !!}
      </div>
    </div>
    </div>
  </section>
  <section id="questions">
      <div class="wrap">
    <div class="row">
      <div class="title col-md-6">
        <h2>Questions & Answers</h2>
      </div>
      <div class="answers drawers col-md-6">
          @php 
          if( have_rows('q_and_a') ): 
          while( have_rows('q_and_a') ): 
          the_row(); 
          $question = get_sub_field('question');
          $answer = get_sub_field('answer');
          @endphp
            <div class="drawer">
                <div class="title">
                  @php echo $question @endphp
                  <i class="fa fa-caret-down"></i>
                </div>
              <div class="content">
                @php echo $answer @endphp
              </div>
            </div>
          @php endwhile; endif; @endphp
      </div>
    </div>
  </div>
  </section>
  <section id="act" class="row">

      
      @if( have_rows('actions') )
      @while( have_rows('actions') )
      @php the_row(); 
      $image = get_sub_field('background_image');
      $page = get_sub_field('page');
      $title = get_sub_field('title');
      $button_text = get_sub_field('button_text');
      @endphp

        <div class="action col-md-4">
        <div class="action-inner" style="background-image: url({{ $image['url'] }})">
                <div class="content">
                <h4>{{ $title }}</h4>
                  <div>
                  <a class="button" href="{{ get_permalink($page) }}">{{ $button_text }}</a>
                  </div>
                </div>
            </div>
        </div>

    @endwhile
    @endif
    
    
    
  </section>
  

  <div id="progressBar"></div>
  </div>

  @endsection