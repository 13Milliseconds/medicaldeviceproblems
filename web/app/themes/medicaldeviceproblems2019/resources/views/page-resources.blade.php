@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @if ( has_post_thumbnail() )
      <div class="banner" data-parallax="scroll" data-image-src="{!! get_the_post_thumbnail_url() !!}"></div>
    @endif

    <section id="pageMenu">
        <div class="wrap">
          <h1>{!! App::title() !!}</h1>
          <a href="#findADevice">Find a Device</a>
          <a href="#questions">Questions and Answers</a>
          <a href="#media">Media</a>
          <a href="#organizations">Organizations</a>
        </div>
    </section>

  <section id="findADevice">
    <div class="wrap">
        <h3>Find a Device</h3>
        <div class="drawers">
        @php 
          if( have_rows('device_drawers') ): 
          while( have_rows('device_drawers') ): 
          the_row(); 
          $title = get_sub_field('title');
          $content = get_sub_field('content');
          $image = get_sub_field('image');
          @endphp
            <div class="deviceDrawer">
                <div class="title">
                  @php echo $title @endphp
                  <i class="fa fa-caret-down"></i>
                </div>
              <div class="content row">
                <div class="col-6">
                  @php echo '<img src="' . $image['url'] . '" />' @endphp
                </div>
                <div class="col-6">
                    @php echo $content @endphp
                </div>
              </div>
            </div>
          @php endwhile; endif; @endphp
          </div>
      </div>
    </section>

    <section id="questions">
        <div class="wrap">
    <div class="row">
        <div class="col-4">
            <h2>Questions & Answers</h2>
        </div>
        <div class="col-8 drawers">
            @php 
          if( have_rows('q_and_a') ): 
          while( have_rows('q_and_a') ): 
          the_row(); 
          $question = get_sub_field('question');
          $answer = get_sub_field('answer');
          @endphp
            <div class="drawer">
                <div class="title">
                  @php echo $question @endphp
                  <i class="fa fa-caret-down"></i>
                </div>
              <div class="content">
                @php echo $answer @endphp
              </div>
            </div>
          @php endwhile; endif; @endphp
        </div>
      </div>
      </div>
    </section>

    <section id="media">
    <div class="wrap">
    <h3>Media</h3>
    @php 
          if( have_rows('media_boxes') ): 
          while( have_rows('media_boxes') ): 
          the_row(); 
          $image = get_sub_field('image');
          $content = get_sub_field('content');
          @endphp
          <div class="mediaBox reveal row">
              <div class="image col-6">
                  @php echo '<img src="' . $image['url'] . '" />' @endphp
              </div>
              <div class="content col-6">
                  @php echo $content @endphp
              </div>
          </div>
      @php endwhile; endif; @endphp
      </div>
    </section>

    <section id="organizations">
    <div class="wrap">
        <h3>Trusted Organizations</h3>
        <div class="">
            @php 
          if( have_rows('trusted_organizations') ): 
          while( have_rows('trusted_organizations') ): 
          the_row(); 
          $logo = get_sub_field('logo');
          $link = get_sub_field('link');
          @endphp
              <a href="{{ $link }}">
                  @php echo '<img src="' . $logo['url'] . '" />' @endphp
              </a>
          @php endwhile; endif; @endphp
        </div>
      </div>
    </section>
  @endwhile
@endsection