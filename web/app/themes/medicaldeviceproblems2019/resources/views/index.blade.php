@extends('layouts.app')

@section('content')
  <div class="news">
    <div class="blog-header">
      <h2>{!! App::title() !!}</h2>
    </div>

    <div class="container">
    @if (!have_posts())
      <div class="alert alert-warning">
        {{ __('Sorry, no results were found.', 'sage') }}
      </div>
      {!! get_search_form(false) !!}
    @endif

    <div class="posts row">
      @while (have_posts()) @php the_post() @endphp
        @include('partials.single-block')
      @endwhile
    </div>

    {!! get_the_posts_navigation() !!}
  </div>
  </div>
@endsection
